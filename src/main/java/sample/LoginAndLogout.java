package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LoginAndLogout {

	static String TextOne= "Reports";
	static String TextTwo= "QnA Maker";
	
	public static void main(String[] args) throws InterruptedException {

		//launch browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe"); 
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://klobotbuilderui-dev.azurewebsites.net/#/login");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
		System.out.println("Browser launched successfully");
		
		//login
		driver.findElementByName("emailId").sendKeys("abbas@klstinc.com");
		driver.findElementByName("pwd").sendKeys("!KLoBot2019");
		driver.findElementByXPath("//button[text()='Login']").click();
		System.out.println("Login done successfully");
		
		//wait to load home page
		Thread.sleep(5000);
		
		//check reports option  coming or not
		String FoundedTextOne = driver.findElementByXPath("//a[text()='Reports']").getText();
		if(TextOne.equals(FoundedTextOne)) {
			System.out.println("Reports option is coming successfully");
		}else {
			System.err.println("Reports option is not coming");
		}
		
		//check QnA maker coming or not
		String FoundedTextTwo = driver.findElementByXPath("//a[text()='QnA Maker']").getText();
		if(TextTwo.equals(FoundedTextTwo)) {
			System.out.println("QnA maker option is coming successfully");
		}else {
			System.err.println("QnA maker option is not coming");
		}
		
		//check user name	
		String FoundedTextThree = driver.findElementByClassName("username").getText();
		System.out.println("Current user name is "+FoundedTextThree);
		
		//logout
		driver.findElementByXPath("//img[@alt='logout']").click();
		System.out.println("Logout done successfully");
		
		driver.close();
		
	}
}
