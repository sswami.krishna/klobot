package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class DeleteBot {

	public static void main(String[] args) throws InterruptedException {

		String deleteBot= "New S Bot";
		
		//launch browser
				System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe"); 
				ChromeDriver driver=new ChromeDriver();
				driver.manage().window().maximize();
				driver.get("https://klobotbuilderui-dev.azurewebsites.net/#/login");
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
				System.out.println("Browser launched successfully");

				//login
				driver.findElementByName("emailId").sendKeys("abbas@klstinc.com");
				driver.findElementByName("pwd").sendKeys("!KLoBot2019");
				driver.findElementByXPath("//button[text()='Login']").click();
				System.out.println("Login done successfully");

				//wait to load home page
				Thread.sleep(5000);
				
				//delete created Bot
				String FoundedBotName = driver.findElementByXPath("//h3[text()='New S Bot']").getText();
				if(FoundedBotName.equals(deleteBot)) {
					driver.findElementByXPath("//h3[text()='New S Bot']/preceding::button[1]").click();
					driver.findElementByXPath("(//button[text()='Delete'])[10]").click();
					System.out.println(deleteBot +" is deleted successfully*");
				}else {
					System.err.println(deleteBot +" is not found to delete");
				}
	}

}
