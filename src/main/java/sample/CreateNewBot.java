package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class CreateNewBot {

	
	public static void main(String[] args) throws InterruptedException {

		//launch browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe"); 
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://klobotbuilderui-dev.azurewebsites.net/#/login");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
		System.out.println("Browser launched successfully");

		//login
		driver.findElementByName("emailId").sendKeys("abbas@klstinc.com");
		driver.findElementByName("pwd").sendKeys("!KLoBot2019");
		driver.findElementByXPath("//button[text()='Login']").click();
		System.out.println("Login done successfully");

		//wait to load home page
		Thread.sleep(5000);

		//create new Bot
		driver.findElementByXPath("//h3[text()='Add a New Bot']").click();
		System.out.println("New Bot created successfully");

		//rename Bot
		driver.findElementById("botname").clear();
		driver.findElementById("botname").sendKeys("New S Bot");
		System.out.println("Bot name changed as *New S Bot*");

		//back to home
		driver.findElementByXPath("//i[@class='fas fa-long-arrow-alt-left']").click();

		//close browser
		driver.close();
		
	}

}
