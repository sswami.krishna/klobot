package sample;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class LaunchingBrowser {

	public static void main(String[] args) {


		//disabel "chrome is being controlled" message
		ChromeOptions options= new ChromeOptions();
		options.addArguments("disable-infobars");

		//launch browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe"); 
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://klobotbuilderui-dev.azurewebsites.net/#/login");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
		
		//message
		System.out.println("Browser launched successfully");

	}
}
